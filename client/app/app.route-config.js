(function () {

    angular
        .module("paApp")
        .config(uirouterAppConfig)
    uirouterAppConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function uirouterAppConfig($stateProvider, $urlRouterProvider) {
        $stateProvider

            .state("home", {
                url: "/home",
                templateUrl: "/app/home/home.html",
                controller: 'HomeCtrl',
                controllerAs: 'ctrl'
            })
            .state("signup", {
                url: "/signup",
                templateUrl: "/app/signup/signup.html",
                controller: 'SignupCtrl',
                controllerAs: 'ctrl',
                authenticate: true
                
            })
            .state("privacy", {
                url: "/privacy",
                templateUrl: "/privacy.html",
                controller: 'HomeCtrl',
                controllerAs: 'ctrl',
                authenticate: true
                
            })
            .state('signup.user', {
                url: '/user',
                templateUrl: '/app/signup/user.html',
                controller: 'SignupCtrl',
                controllerAs: 'ctrl'
            })
  
            .state('signup.clients', {
                url: '/clients',
                templateUrl: '/app/signup/clients.html',
                controller: 'SignupCtrl',
                controllerAs: 'ctrl'
            })
    
            .state('signup.schedules', {
                url: '/schedules',
                templateUrl: '/app/signup/schedules.html',
                controller: 'SignupCtrl',
                controllerAs: 'ctrl'
            })
            .state('signup.report', {
                url: '/report',
                templateUrl: '/app/signup/report.html',
                controller: 'SignupCtrl',
                controllerAs: 'ctrl'
            })
            .state("login", {
                url: "/login",
                templateUrl: 'app/login/login.html',
                controller: 'LoginCtrl',
                controllerAs: 'ctrl',
                resolve: {
                    user: function (PassportSvc) {
                        return PassportSvc.userAuth()
                            .then(function (result) {
                                return result.data.user;
                            })
                            .catch(function (err) {
                                return err;
                            });
                        }
                    },
            })
            .state("welcome", {
                url: "/welcome",
                templateUrl: 'app/welcome/welcome.html',
                controller: 'WelcomeCtrl',
                controllerAs: 'ctrl',
                resolve: {
                    user: function (PassportSvc) {
                        return PassportSvc.userAuth()
                            .then(function (result) {
                                return result.data.user;
                            })
                            .catch(function (err) {
                                console.log(err);
                                $rootScope.$broadcast('event:auth-error', false);
                                return err;
                            });
                        }
                    },
            })
            .state("welcome.add", {
                url: "/add",
                templateUrl: 'app/welcome/add.html',
                controller: 'WelcomeCtrl',
                controllerAs: 'ctrl',
                resolve: {
                    user: function (PassportSvc, $rootScope) {
                        return PassportSvc.userAuth()
                            .then(function (result) {
                                return result.data.user;
                            })
                            .catch(function (err) {
                                console.log(err);
                                $rootScope.$broadcast('event:auth-error', false);
                                return err;
                            });
                        }
                    },

            })
            .state("welcome.edit", {
                url: "/edit",
                templateUrl: 'app/welcome/edit.html',
                controller: 'WelcomeCtrl',
                controllerAs: 'ctrl',
                resolve: {
                    user: function (PassportSvc, $rootScope) {
                        return PassportSvc.userAuth()
                            .then(function (result) {
                                return result.data.user;
                            })
                            .catch(function (err) {
                                console.log(err);
                                $rootScope.$broadcast('event:auth-error', false);
                                return err;
                            });
                        }
                    },
            })
            .state("gwelcome", {
                url: "/gwelcome",
                templateUrl: 'app/gwelcome/gwelcome.html',
                controller: 'GwelcomeCtrl',
                controllerAs: 'ctrl',
                resolve: {
                    user: function (PassportSvc) {
                        return PassportSvc.userAuth()
                            .then(function (result) {
                                return result.data.user;
                            })
                            .catch(function (err) {
                                console.log(err);
                                $rootScope.$broadcast('event:auth-error', false);
                                return err;
                            });
                        }
                    },
            })
            .state("gwelcome.gcal", {
                url: "/gcal",
                templateUrl: 'app/gwelcome/gcal.html',
                controller: 'GwelcomeCtrl',
                controllerAs: 'ctrl',
                resolve: {
                    user: function (PassportSvc, $rootScope) {
                        return PassportSvc.userAuth()
                            .then(function (result) {
                                return result.data.user;
                            })
                            .catch(function (err) {
                                console.log(err);
                                $rootScope.$broadcast('event:auth-error', false);
                                return err;
                            });
                        }
                    },
            })
            .state("gwelcome.gadd", {
                url: "/gadd",
                templateUrl: 'app/gwelcome/gadd.html',
                controller: 'GwelcomeCtrl',
                controllerAs: 'ctrl',
                resolve: {
                    user: function (PassportSvc, $rootScope) {
                        return PassportSvc.userAuth()
                            .then(function (result) {
                                return result.data.user;
                            })
                            .catch(function (err) {
                                console.log(err);
                                $rootScope.$broadcast('event:auth-error', false);
                                return err;
                            });
                        }
                    },
            })
            .state("gwelcome.gedit", {
                url: "/gedit",
                templateUrl: 'app/gwelcome/gedit.html',
                controller: 'GwelcomeCtrl',
                controllerAs: 'ctrl',
                resolve: {
                    user: function (PassportSvc, $rootScope) {
                        return PassportSvc.userAuth()
                            .then(function (result) {
                                return result.data.user;
                            })
                            .catch(function (err) {
                                console.log(err);
                                $rootScope.$broadcast('event:auth-error', false);
                                return err;
                            });
                        }
                    },
            });

        $urlRouterProvider.otherwise("/home");
    }

})();