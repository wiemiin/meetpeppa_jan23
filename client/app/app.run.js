angular.module("paApp").run(function ($rootScope, $state, PassportSvc) {
  
  $rootScope.$on('event:auth-error', function (event, data) {
    console.log("event:auth-error"+ data); // 'Emit!'
    $state.go("login");
  });
});