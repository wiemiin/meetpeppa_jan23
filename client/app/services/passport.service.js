(function() {
    angular
      .module('paApp')
      .service('PassportSvc', PassportSvc);
  
    PassportSvc.$inject = [ "$http" ];
  
    function PassportSvc($http) {
      var self = this;
  
      self.login = login;
      self.userAuth = userAuth;
  
      function userAuth() { 
        return $http.get(
          '/user/auth',
        );
      }
  
      function login(user) { 
        return $http.post(
          '/login',
          user
        );
      }
    }
  })();