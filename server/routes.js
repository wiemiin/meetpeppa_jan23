const WELCOME = "/#!/gwelcome/gcal";
const LOGIN = "/#!/login";

var User = require('./models/user');
var Client = require('./models/client');
var GClient = require('./models/gcal-client');
var Booking = require('./models/booking');
var GcalBooking = require('./models/gcal-event');
var Twilio = require('twilio');

var moment = require('moment');
moment().format();
var momentTimeZone = require('moment-timezone');
var schedule = require('node-schedule');

var bcrypt = require('bcrypt-nodejs');
var config = require('./config');


module.exports = function (auth, app, passport) {

  var generateHash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
  };

  //LOCAL USER

  app.post('/login', passport.authenticate('local'), function (req, res) {
    console.log("passport user" + req.user);
    res.status(200).send({
      user: req.user
    });

  });

  app.get('/user/auth', auth.isAuthenticated, function (req, res, next) {
    if (req.user) {
      res.status(200).json({
        user: req.user
      });
    } else {
      res.sendStatus(401);
    }
  });

  //GOOGLE USER

  app.get("/oauth/google", passport.authenticate("google", {
    scope: ["email", "profile",
      "https://www.googleapis.com/auth/calendar",
      "https://www.googleapis.com/auth/contacts"
    ],
    accessType: 'offline',
    prompt: 'consent'
  }));

  app.get("/oauth/google/callback", passport.authenticate("google", {
    successRedirect: WELCOME,
    failureRedirect: LOGIN
  }));

  app.get('/logout', function (req, res) {
    req.logOut();
    res.redirect('/');
  });

  /**
   * Register USER
   */
  app.post("/api/user", function (req, res) {
    const user = req.body;
    console.log(user);

    User.findOne({
        'local.email': user.email
      },
      function (err, result) {
        if (err) {
          console.log(err);
          handleError(err, res);
          return;
        }
        if (result) {
          res.status(500).send("Email already exists in database");
        } else {

          const localsms = new Twilio(
            config.Twilio_sid,
            config.Twilio_token
          );

          localsms.messages.create({
            to: "+65" + user.mobile,
            from: "Peppa",
            body: "Hi " + user.name + ", I'm Peppa, great meeting you!"
          }).then((message) => console.log(message.sid));

          var newUser = new User();
          newUser.local.password = generateHash(user.password);
          newUser.local.email = user.email;
          newUser.local.name = user.name;
          newUser.local.mobile = user.mobile;
          newUser.save(function (err, result) {
            res.status(201).send("User added to database");
          });
        }

      });
  });


  /**
   * Register CLIENT
   */
  app.post("/api/client", function (req, res) {
    const client = req.body;
    console.log(client);

    Client.findOne({
        $and: [{
            firstname: client.firstname
          },
          {
            lastname: client.lastname
          },
          {
            userid: client.userid
          }
        ]
      },
      function (err, result) {
        if (err) {
          console.log(err);
          handleError(err, res);
          return;
        }
        if (result) {
          res.status(500).send({
            msg: "Client already exists in database"
          });
        } else {
          var newClient = new Client();
          newClient.firstname = client.firstname;
          newClient.lastname = client.lastname;
          newClient.mobile = client.mobile;
          newClient.userid = client.userid;
          console.log(newClient.userid);
          newClient.save(function (err, result) {
            res.status(201).send("Client added to database");
            console.log(result)
          });
        }

      });

  });

  /**
   * EDIT client
   */

  app.get("/api/client/:id", function (req, res) {
    const id = req.params.id;
    console.log(req.params.id);

    Client.findOne({
      "_id": req.params.id
    }, (err, result) => {
      if (err) {
        console.log(err);
        handleError(err, res);
        return;
      }

      res.status(200).json({
        msg: 'Start editing'
      });
    });
  });

  /**
   * SAVE client
   */

  app.put("/api/client/:id", function (req, res) {

    console.log(req.params.id);
    console.log(req.body.client);

    Client.updateOne({
      "_id": req.params.id
    }, {
      $set: req.body.client
    }, (err, result) => {
      if (err) {
        console.log(err);
        handleError(err, res);
        return;
      }

      res.status(200).json({
        msg: 'Successfully saved'
      });
    });
  });

  /**
   * DELETE client
   */


  app.delete("/api/client/:id", function (req, res) {
    console.log(req.params.id);

    Client.deleteOne({
      "_id": req.params.id
    }, function (err, result) {
      if (err) {
        console.log(err);
        handleError(err, res);
        return;
      }
      res.status(200).json({
        msg: 'Successfully deleted'
      });
    });

  });

  /**
   * SEARCH client name
   */

  app.get("/api/client", function (req, res) {

    console.log("Search > " + JSON.stringify(req.query));
    var keyword = req.query.keyword;
    var filterby = req.query.filterby;

    Client.find({
      $and: [{
          "firstname": new RegExp('^' + keyword + '$', "i")
        },
        {
          userid: filterby
        },
      ]
    }, null, {
      sort: {
        firstname: 1
      }
    }, (err, result) => {
      if (err) {
        console.log(err);
      }
      res.status(200).json(result);
      console.log("Result is... " + result)

    });
  });


  /**
   * SEARCH all clients
   */

  app.get("/client", function (req, res) {
    var user_id = req.query.id;

    Client.find({
      userid: user_id
    }, null, {
      sort: {
        firstname: 1
      }
    }, (err, result) => {
      if (err) {
        console.log(err);
      }
      res.status(200).json(result);

    });
  });

  /**
   * Register local BOOKING
   */
  app.post("/api/booking", function (req, res) {
    console.log("Inside booking " + req.body)
    const booking = req.body;
    //console.log("Inside booking " +  JSON.stringify(booking));

    var newBooking = new Booking();
    newBooking.training = booking.training
    newBooking.firstname = booking.client.firstname;
    newBooking.lastname = booking.client.lastname;
    newBooking.datetime = booking.datetime;
    newBooking.notification = booking.notification;
    newBooking.clientid = booking.clientid;
    newBooking.userid = booking.userid;


    var userInfo = req.body.user.local; //find user's name 

    var bookingDate = req.body.datetime; // actual time
    var notification = req.body.notification; // notification time

    // convert UTC time to local time
    var bookingTime = momentTimeZone.tz(bookingDate, "Asia/Singapore").format('llll');

    var smsTime = new Date();
    //substract notification from booking time to send notification on that time
    if (notification == "1 minute") {
      bookingDate = moment(bookingDate).subtract(1, 'minutes');
    } else if (notification == "12 hours") {
      bookingDate = moment(bookingDate).subtract(12, 'hours');
    } else if (notification == "24 hours") {
      bookingDate = moment(bookingDate).subtract(24, 'hours');
    } else if (notification == "48 hours") {
      bookingDate = moment(bookingDate).subtract(48, 'hours');
    }

    var converted = momentTimeZone.tz(bookingDate, "Asia/Singapore").format(); // converted date and time to Local
    var textmessage = " Hi " + newBooking.firstname + ", get ready for " + newBooking.training + " with " + userInfo.name + "  on " + bookingTime;

    // create and send sms on scheduled time
    var sms = schedule.scheduleJob(converted, function () {
      const localsms = new Twilio(
        config.Twilio_sid,
        config.Twilio_token
      );
      localsms.messages.create({
        body: textmessage,
        to: "+65" + booking.client.mobile,
        from: "Peppa"
      }, function (err, sms) {
        if (err)
          console.log(err);

        console.log("Sending to user " + newBooking.firstname + " " + booking.client.mobile);
        //process.stdout.write(sms.sid);
      });
    });

    newBooking.save(req.body, function (error, result) {
      console.log(req.body);
      if (error) {
        console.log(error);
      }
      res.status(201).send("New booking added to database");
      console.log(result);


    });
  });

  /**
   * Register google BOOKING
   */

  app.get("/api/gcal", function (req, res) {

    var gemail = req.query.email;

    GcalBooking.find({
      email: gemail
    }, null, {
      sort: {
        datetime: 1
      }
    }, (err, result) => {
      if (err) {
        console.log(err);
      }
      res.status(200).json(result);

    });


  })

 



  /**
   * SEARCH bookings by name
   */

  app.get("/api/booking", function (req, res) {

    var keyword = req.query.keyword;
    var filterby = req.query.filterby;

    console.log("Search booking by name > " + req.query.keyword);

    Booking.find({
      $and: [{
          "firstname": new RegExp('^' + keyword + '$', "i")
        },
        {
          userid: filterby
        },
      ]
    }, null, {
      sort: {
        firstname: 1
      }
    }, (err, result) => {
      if (err) {
        console.log(err);
      }
      res.status(200).json(result);
      //console.log(result[0].datetime);

    });
  });


  /**
   * SEARCH all bookings
   */

  app.get("/booking", function (req, res) {

    var user_id = req.query.id;

    console.log("Search > " + JSON.stringify(req.query));
    console.log("Search ID > " + JSON.stringify(req.query.id));

    Booking.find({
      userid: user_id
    }, (err, result) => {
      if (err) {
        console.log(err);
      }
      res.status(200).json(result);
    });
  });

  /**
   * EDIT AND SAVE booking
   */

  app.put("/api/booking/:id", function (req, res) {

    console.log(req.params.id);
    console.log(req.body.booking);

    Booking.updateOne({
      "_id": req.params.id
    }, {
      $set: req.body.booking
    }, (err, result) => {
      if (err) {
        console.log(err);
        handleError(err, res);
        return;
      }

      res.status(200).json({
        msg: 'Successfully saved'
      });
    });
  });

  /**
   * DELETE booking
   */

  //route
  app.delete("/api/booking/:id", function (req, res) {
    console.log(req.params.id);
    //const id = req.params.id.toString(); //to convert type 
    //console.log("Deleting " + id);

    //controller
    Booking.deleteOne({
      "_id": req.params.id
    }, function (err, result) {
      if (err) {
        console.log(err);
        handleError(err, res);
        return;
      }
      res.status(200).json({
        msg: 'Successfully deleted'
      });
    });

  });

  /**
   * Register Google CLIENT
   */
  app.post("/api/gclient", function (req, res) {
    const gclient = req.body;
    console.log(gclient);

    GClient.findOne({
        name: gclient.name
      },
      function (err, result) {
        if (err) {
          console.log(err);
          handleError(err, res);
          return;
        }
        if (result) {
          res.status(500).send({
            msg: "Client already exists in database"
          });
        } else {
          var newGclient = new GClient();
          newGclient.name = gclient.name;
          newGclient.mobile = gclient.mobile;
          newGclient.userid = gclient.userid;
          console.log(newGclient.userid);
          newGclient.save(function (err, result) {
            res.status(201).send("Google client added to database");
            console.log(result)
          });
        }

      });

  });

  /**
   * SEARCH all Google clients
   */

  app.get("/gclient", function (req, res) {
    var user_id = req.query.id;

    GClient.find({
      userid: user_id
    }, null, {
      sort: {
        name: 1
      }
    }, (err, result) => {
      if (err) {
        console.log(err);
      }
      res.status(200).json(result);

    });
  });


  /**
   * EDIT gclient
   */

  app.get("/api/gclient/:id", function (req, res) {
    const id = req.params.id;
    console.log(req.params.id);

    GClient.findOne({
      "_id": req.params.id
    }, (err, result) => {
      if (err) {
        console.log(err);
        handleError(err, res);
        return;
      }

      res.status(200).json({
        msg: 'Start editing'
      });
    });
  });

  /**
   * SAVE gclient
   */

  app.put("/api/gclient/:id", function (req, res) {

    console.log(req.params.id);
    console.log(req.body.gclient);

    GClient.updateOne({
      "_id": req.params.id
    }, {
      $set: req.body.gclient
    }, (err, result) => {
      if (err) {
        console.log(err);
        handleError(err, res);
        return;
      }

      res.status(200).json({
        msg: 'Successfully saved'
      });
    });
  });

  /**
   * DELETE gclient
   */


  app.delete("/api/gclient/:id", function (req, res) {
    console.log(req.params.id);

    GClient.deleteOne({
      "_id": req.params.id
    }, function (err, result) {
      if (err) {
        console.log(err);
        handleError(err, res);
        return;
      }
      res.status(200).json({
        msg: 'Successfully deleted'
      });
    });

  });

  /**
   * SAVE Google calendar
   */

  app.put("/api/gcal/:id", function (req, res) {
    
        console.log(req.params.id);
        console.log(req.body.cal);
    
        GcalBooking.updateOne({
          "_id": req.params.id
        }, {
          $set: req.body.cal
        }, (err, result) => {
          if (err) {
            console.log(err);
            handleError(err, res);
            return;
          }
    
          res.status(200).json({
            msg: 'Successfully saved'
          });
        });
      });
    
    
      /**
       * DELETE Google calendar
       */
    
    
      app.delete("/api/gcal/:id", function (req, res) {
        console.log(req.params.id);
    
        GcalBooking.deleteOne({
          "_id": req.params.id
        }, function (err, result) {
          if (err) {
            console.log(err);
            handleError(err, res);
            return;
          }
          res.status(200).json({
            msg: 'Successfully deleted'
          });
        });
    
      });

 

};