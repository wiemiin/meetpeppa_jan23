var schedule = require('node-schedule');
const googleapi = require('googleapis');
const OAuth2 = googleapi.auth.OAuth2;
var User = require('./models/user');
var Client = require('./models/client');
var GcalBooking = require('./models/gcal-event');
var GClient = require('./models/gcal-client');

async function loadPeopleFromGoogle(err, result) {
    console.log("Running ...")
    if (err) {
        console.log(err);
        return;
    }

    for (let x = 0; x < result.length; x++) {
        
        const token = result[x].google.token;
        const _refreshToken = result[x].google.refreshToken;
        console.log("Token: ", result[x].google.token);
        console.log("Email: ", result[x].google.email);

        try {
            //var newToken = await refreshToken(token, refreshToken);
            //console.log(">>> new token: ", newToken);
            let peoples = await getGooglePeople(token, _refreshToken);
            if (!peoples) {
                continue
            }
            console.log(">>> peoples = ", peoples);
            console.log(">>> peoples/email = ", peoples[0].emailAddresses[0].value);
        
        } catch (e) {
            console.log("error: %d", x, e);
        }
    }
}

var j = schedule.scheduleJob('*/1 * * * *', function () {

    User.find({'google': { $exists: true}}, loadPeopleFromGoogle);

    });

    var refreshToken = function (token, refreshToken) {

        const oauth2Client = new OAuth2(config.clientID, config.clientSecret, config.callbackURL);
        //Set the access token that we got
        oauth2Client.credentials = {
            //access_token: token,
            refresh_token: refreshToken
        };

        return new Promise(function (resolve, reject) { 
            console.log("calling refresh token");
            oauth2Client.refreshAccessToken(
                function(err, token) {
                    if (err)
                    return reject(err);
                    else 
                    return resolve(token);
                }
            )
        })
    }

    function getGooglePeople(token, refreshToken) {

        const oauth2Client = new OAuth2(config.clientID, config.clientSecret, config.callbackURL);
        //Set the access token that we got
        oauth2Client.credentials = {
            access_token: token,
            refresh_token: refreshToken
        };

        const listConfig = {
            resourceName: 'people/me',
            "requestMask.includeField": "person.names,person.phoneNumbers,person.emailAddresses" //no spaces
        };
        const peopleAPI = googleapi.people({ version: "v1", auth: oauth2Client});

        console.log("Here")
        return new Promise(function (resolve, reject) {
            peopleAPI.people.connections.list(listConfig,
                function(error, result) {
                    console.log("Error = ", error)
                    if (error) {
                        return reject(error);
                    }
                    console.log(result)
                    return resolve(result.connections);
                });
            })
    }

