"use strict";
console.log("Starting...");

require('dotenv').config();

var express = require("express");
var compression = require('compression')
var app = express();
app.use(compression());

//var morgan       = require('morgan');
var cookieParser = require('cookie-parser');
process.setMaxListeners(0);

var bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());

const fs = require('fs');
const path = require('path');
const uuid = require('uuid/v4');

console.log(__dirname);
console.log(__dirname + "/../client/");
const NODE_PORT = process.env.PORT;

//app.use(express.logger('dev')); // log every request to the console
//app.use(express.cookieParser()); // read cookies (needed for auth)

var session = require('express-session');
var passport = require('passport');
var flash = require('connect-flash');
var config = require('./config');
var PORT = config.PORT;
var mongoose = require('mongoose');
// Connect to DB
mongoose.connect(config.Mongodb_url, {
    useMongoClient: true
});
mongoose.Promise = global.Promise;
console.log(mongoose.connection.readyState);

var moment = require('moment');
moment().format();
var momentTimeZone = require('moment-timezone');
var schedule = require('node-schedule');
var GcalBooking = require('./models/gcal-event');
var User = require('./models/user');
var Twilio = require('twilio');

//var googleSchedule = schedule.scheduleJob('*/1 * * * *', function () {
    
    GcalBooking.find({'gcalId': { $exists: true}} , (err, result) => {
            if (err) {
              console.log(err);
            }
            console.log(result); 

            for (let x = 0; x < result.length; x++) {
                //console.log(result[x].summary)
                //console.log(result[x].datetime)
                //console.log(result[x].mobile)
                
                var gbookingDate = result[x].datetime;
                var gbookingNotify = result[x].notification;
                var gbookingMobile = result[x].mobile;
                var gbookingClientname = result[x].summary;
                var gbookingTraining = result[x].training;
                var gbookingTime = momentTimeZone.tz(gbookingDate, "Asia/Singapore").format('llll');
            
                //console.log(gbookingDate)
            
                var gsmsTime = new Date();
                //substract notification from booking time to send notification on that time
                if (gbookingNotify == "1 minute") {
                  gbookingDate = moment(gbookingDate).subtract(1, 'minutes');
                } else if (gbookingNotify == "12 hours") {
                  gbookingDate = moment(gbookingDate).subtract(12, 'hours');
                } else if (gbookingNotify == "24 hours") {
                  gbookingDate = moment(gbookingDate).subtract(24, 'hours');
                } else if (gbookingNotify == "48 hours") {
                  gbookingDate = moment(gbookingDate).subtract(48, 'hours');
                }
            
                var gconverted = momentTimeZone.tz(gbookingDate, "Asia/Singapore").format();
                var gtextmessage = " Hi " + gbookingClientname + ", get ready for " + gbookingTraining + "  on " + gbookingTime;

            
                var g_sms = schedule.scheduleJob(gconverted, function () {
                  const googlesms = new Twilio(
                    config.Twilio_sid,
                    config.Twilio_token
                  );
                  googlesms.messages.create({
                    body: gtextmessage,
                    to: "+65" + gbookingMobile,
                    from: "Peppa"
                  }, function (err, g_sms) {
                    if (err)
                      console.log(err);
            
                    console.log("Sending to Google client " + gbookingClientname + " " + gbookingMobile);
                    //process.stdout.write(sms.sid);
                  });
                });
            
              }
        });
    //})

app.use(session({
    secret: config.SECRET,
    resave: false,
    saveUninitialized: false,
}));

app.use(express.static(__dirname + "/../client/"));

app.use(flash());
app.use(passport.initialize());
app.use(passport.session());

var auth = require('./auth')(app, passport);
require('./routes')(auth, app, passport);

app.use(function (req, res) {
    res.send("<h1>Page Not Found</h1>");
});

app.listen(NODE_PORT, function () {
    console.log("Web App started at " + NODE_PORT);
});

module.exports = app;
